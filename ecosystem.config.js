const getenv = require('getenv');

module.exports = {
    name: `${getenv('PROJECT_NAME')}-main-fe`,
    namespace: getenv('NAMESPACE'),
    cwd: __dirname,
    script: './node_modules/npm/bin/npm-cli.js',
    args: 'start',
};
