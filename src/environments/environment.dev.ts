
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// This serves as the base for the dev environment
export const environment = {
    production: false,
    hmr: false,
    API: 'http://localhost:3000',
    mssoEnabled: true,
    mfaEnabled: false,
    mfaDisabled: true,
    mssoDisabled: false,
    mssoFederatedOnly: true,
    // set to true for dev mode (uncomment for testing Authentication package)
    authDevMode: false,
    devModeEnabled: false,
    // refresh token defaults
    jwtTokenExpiresIn: parseInt('120', 10),
    refreshTokenExpiresIn: parseInt('600', 10),
    // session timeout defaults
    sessionTimeoutDisabled: false,
    sessionTimeoutWarningTime: parseInt('120', 10),
    authUserProfileReturnRoute: '/user-profile',
    displayName: 'MOS-MAIN-FE',
    appName: 'MOS-MAIN-FE',
    rpid: 'https://localdev-main.mercer.com',
    mssoLoginPost: 'https://globalidentity1-uat.mercer.com/GlobalSSOUI/GSSOIdp/Authenticate/LoginPost',
    mssoFailureUrl: 'http://localhost:3000/msso/login/failed',
    loginSuccessRoute: '/dashboard',
    loginRoute: '/login',
    mssoSupportEmail: 'mssosupport@mercer.com',
    mfaSupportEmail: 'mfasupport@mercer.com',
    startingMfaLocale: 'en-US',
};
