export const environment = {
  production: true,
  hmr: false,
  API: '/api/v1',   // '$(API)'
  mssoEnabled: true,
  mfaDisabled: true,
  mfaEnabled: false,
  mssoDisabled: false,
  mssoFederatedOnly: false,
  authDevMode: false,
  devModeEnabled: false,
  // Refresh Token Defaults
  jwtTokenExpiresIn: parseInt('$(JWT_TOKEN_EXPIRES_IN)', 10),
  refreshTokenExpiresIn:  parseInt('$(REFRESH_TOKEN_EXPIRES_IN)', 10),
  // Session Timeout Defaults
  sessionTimeoutDisabled: false,
  sessionTimeoutWarningTime:  parseInt('$(SESSION_TIMEOUT_WARNING_TIME)', 10),
  authUserProfileReturnRoute: '$(AUTH_USER_PROFILE_RETURN_ROUTE)',
  displayName: '$(DISPLAY_NAME)',
  appName: '$(APP_NAME)',
  rpid: '$(RPID)',
  mssoLoginPost: '$(MSSO_LOGIN_POST)',
  mssoFailureUrl: '$(MSSO_FAILURE_URL)',
  loginSuccessRoute: '$(LOGIN_SUCCESS_ROUTE)',
  loginRoute: '$(LOGIN_ROUTE)',
  mssoSupportEmail: '$(MSSO_SUPPORT_EMAIL)',
  mfaSupportEmail: '$(MFA_SUPPORT_EMAIL)',
  startingMfaLocale: '$(STARTING_MFA_LOCALE)',
};
