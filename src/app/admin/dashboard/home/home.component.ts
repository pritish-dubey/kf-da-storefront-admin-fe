import { Component, OnInit } from '@angular/core';
import { getBackendUrl  } from '../../../shared/utils/environment/environment'
interface Place {
  imgSrc: string;
  name: string;
  description: string;
  //charge: string;
  //location: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  places: Array<Place> = [];
  constructor() {
    console.log(getBackendUrl())
  }
  ngOnInit() {
    this.places = [
      {
        imgSrc: 'assets/images/card1.jpg',
        name: 'Job Postings',
        description: `Over 5,000 standardized financial, 
        supplemental, segment, ratio, and industry-specific 
        data items on an annual, quarterly, semi-annual, 
        last twelve month and year-to-date basis`,
        //charge: '$899/night',
        //location: 'Barcelona, Spain'
      },
      {
        imgSrc: 'assets/images/card2.jpg',
        name: 'Job Library',
        description: `Over 5,000 standardized financial, 
        supplemental, segment, ratio, and industry-specific 
        data items on an annual, quarterly, semi-annual, 
        last twelve month and year-to-date basis`,
        //charge: '$1,119/night',
        //location: 'London, UK'
      },
      {
        imgSrc: 'assets/images/card3.jpg',
        name: 'Job Analytics',
        description: `Over 5,000 standardized financial, 
        supplemental, segment, ratio, and industry-specific 
        data items on an annual, quarterly, semi-annual, 
        last twelve month and year-to-date basis`,
        //charge: '$459/night',
        //location: 'Milan, Italy'
      }
    ];
  }
}
