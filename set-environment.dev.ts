// Load node modules
const colors = require('colors');
require('dotenv-defaults').config();
const fs = require('fs');
// Configure Angular `environment.ts` file path
const targetPath = './src/environments/environment.dev.ts';
// `./src/environments/environment.dev.ts` file structure
const envConfigFile = `
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses \`environment.ts\`, but if you do
// \`ng build --env=prod\` then \`environment.prod.ts\` will be used instead.
// The list of which env maps to which file can be found in \`.angular-cli.json\`.
// This serves as the base for the dev environment
export const environment = {
    production: false,
    hmr: false,
    API: '${process.env.API}',
    mssoEnabled: true,
    mfaEnabled: false,
    mfaDisabled: true,
    mssoDisabled: false,
    mssoFederatedOnly: true,
    // set to true for dev mode (uncomment for testing Authentication package)
    authDevMode: false,
    devModeEnabled: false,
    // refresh token defaults
    jwtTokenExpiresIn: parseInt('${process.env.JWT_TOKEN_EXPIRES_IN}', 10),
    refreshTokenExpiresIn: parseInt('${process.env.REFRESH_TOKEN_EXPIRES_IN}', 10),
    // session timeout defaults
    sessionTimeoutDisabled: false,
    sessionTimeoutWarningTime: parseInt('${process.env.SESSION_TIMEOUT_WARNING_TIME}', 10),
    authUserProfileReturnRoute: '${process.env.AUTH_USER_PROFILE_RETURN_ROUTE}',
    displayName: '${process.env.DISPLAY_NAME}',
    appName: '${process.env.APP_NAME}',
    rpid: '${process.env.RPID}',
    mssoLoginPost: '${process.env.MSSO_LOGIN_POST}',
    mssoFailureUrl: '${process.env.MSSO_FAILURE_URL}',
    loginSuccessRoute: '${process.env.LOGIN_SUCCESS_ROUTE}',
    loginRoute: '${process.env.LOGIN_ROUTE}',
    mssoSupportEmail: '${process.env.MSSO_SUPPORT_EMAIL}',
    mfaSupportEmail: '${process.env.MFA_SUPPORT_EMAIL}',
    startingMfaLocale: '${process.env.STARTING_MFA_LOCALE}',
};
`;

console.log(colors.magenta('The file `environment.ts` will be written with the following content: \n'));
console.log(colors.grey(envConfigFile));

fs.writeFile(targetPath, envConfigFile, (err) => {
    if (err) {
        throw console.error(err);
    } else {
        console.log(colors.magenta(`Angular environment.dev.ts file generated correctly at ${targetPath} \n`));
    }
});
